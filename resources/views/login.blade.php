<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NUTELAS'S-DEV</title>
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">

<body>

 <div class="background"></div>

    <section id="conteudo-view" class="login">

            <h1>Nutela's DEV</h1>
            <h3> Os desenvolvedores nutela</h3>

            {!! Form::open(['route' => 'user.login', 'method' => 'post']) !!}
            <p>Acesse nosso sistema</p>

        <label>
            {!! Form::text('username', null, ['class' => 'input', 'placeholder' => 'Usuário' ]) !!}
        </label>

        <label>
            {!! Form::password('password', ['placeholder' => 'Senha']) !!}
        </label>

        {!! Form::submit('Entrar') !!}

            {!! Form::close()!!}
    </section>



</body>
</html>


