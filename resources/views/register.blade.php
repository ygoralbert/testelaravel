<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NUTELAS'S-DEV</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
<body>

<div class="flex-center position-ref full-height">
    <div class="top-right links">
        <a href="/">Home</a>
        <a href="/login">Login</a>
    </div>

    <div class="content">
        <div class="title m-b-md">
            <p>Register</p>
        </div>
        <div class="row">
            <div>
                <form>

                <label>CPF</label>
                <input type="text" title="cpf"><br>

                <label>Nome</label>
                <input type="text" title="name"><br>

                <label>Celular</label>
                <input type="texte" title="phone"><br>

                <label>Nascimento</label>
                <input type="date" title="birth"><br>

                <label>Gênero</label>
                <input type="text" title="gender"><br>

                <label>Email</label>
                <input type="text" title="email"><br>

                <label>Senha</label>
                <input type="password" title="password"><br>

                </form>
            </div>

            <button type="submit">Registrer</button>
        </div>
    </div>
</div>
</body>
</html>