<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Exception;
use Auth;


class DashboardController extends Controller
{

    private $repository;
    private $validator;

    public function index(){

        return "Dashboard usuario";
    }

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }
    public function auth(Request $request){

        $data = [
            'email'     => $request->get('username'),
            'password'  => $request->get('password')

        ];\

        try {

             if (env('PASSWORD_HASH')) {

                Auth::attempt($data, false);

            } else {

                $user = $this->repository->findWhere($data)->first();

                if (!$user) {
                    throw new Exception("As Credenciais informadas são inválidas!");
                }

                Auth::login($user);
            }
                return redirect()->route('user.dashboard');

            }
        catch (Exception $e){
                return $e->getMessage();
            }


    }

}
