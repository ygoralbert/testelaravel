<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'cpf'           =>  '11312998322',
            'name'          =>  'someone',
            'phone'         =>  '1562344534',
            'birth'         =>  '1990-11-12',
            'gender'        =>  'M',
            'email'         =>  'someone@someone.com',
            'password'      =>  env('PASSWORD_HASH') ? bcrypt('123456') : '123456',


        ]);
    }
}
