<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
            $table->increments('id');

            $table->char('cpf', 11)->nullable();
            $table->string('name', 58);
            $table->char('phone', 11)->nullable();
            $table->date('birth')->nullable();
            $table->char('gender', 1)->nullable();
            $table->text('notes')->nullable();

            //auth data
            $table->string('email', 80)->unique();
            $table->char('password', 254)->nullable();

            //permission
            $table->string('status')->default('active');
            $table->string('permission')->default('app.active');

            $table->rememberToken();

            $table->timestamps();
		});
	}

	/**
     * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function(Blueprint $table) {
        });

        Schema::drop('users');
	}
}
